# Acruickshank - Final Project INFO3130

Displays:

- Time
- Date
- Canvas

All will refresh with page refreshes, also canvas will refresh if page is resized at all.

The balls are objects that are animated and can be interacted with.

### Installation 

- Download repository
- Unzip
- Run the index.html

*note: requires node.js

### License Note

I chose an MIT license for this project for a few reasons:

- first of which is the as is nature of this project as it most likely won't have any future developments or updates to it. 
- second of which is that the base of this project isn't propiertary nor does it feature any propiertary software, features, and or code.
- third of which is that the scope of code is very limited and replicating and or creating the project from scratch is trivial
- fourth of which is that this license allows me to give back to programming community at large
